locals {
  sns_topic_arn = var.existing_topic != "" ? var.existing_topic : var.create_topic ? aws_sns_topic.ecr_sns[0].arn : null
}

resource "aws_cloudwatch_event_rule" "push" {
  count       = var.create_event_rule ? 1 : 0
  name        = "capture-${var.name}-push"
  description = "Capture each image push for ${var.name}"

  event_pattern = <<EOF
{
  "detail-type": [
    "ECR Image Action"
  ],
  "detail": {
    "repository-name": ["${var.name}"],
    "action-type": ["PUSH"],
    "result": ["SUCCESS"]
  }
}
EOF
}

resource "aws_cloudwatch_event_target" "sns" {
  count = var.create_topic ? var.create_event_rule ? 1 : 0 : 0
  rule  = aws_cloudwatch_event_rule.push[0].name
  arn   = local.sns_topic_arn
}

resource "aws_sns_topic" "ecr_sns" {
  count = var.create_topic ? var.create_event_rule ? 1 : 0 : 0
  name  = "${var.name}-push-topic"
}

output "sns_topic_arn" {
  value = local.sns_topic_arn
}

resource "aws_sns_topic_policy" "default" {
  count  = var.create_topic ? var.create_event_rule ? 1 : 0 : 0
  arn    = local.sns_topic_arn
  policy = data.aws_iam_policy_document.sns_topic_policy.json
}

data "aws_iam_policy_document" "sns_topic_policy" {
  statement {
    effect  = "Allow"
    actions = ["SNS:Publish"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }

    resources = [local.sns_topic_arn == null ? "" : local.sns_topic_arn]
  }
}