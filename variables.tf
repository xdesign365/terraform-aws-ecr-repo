variable "name" {}
variable "image_tag" {
  default = "latest"
}

variable "create_repository" {
  default = true
}

variable "create_event_rule" {
  default = true
}

variable "create_rollout_rule" {
  default = true
}

variable "create_lifecycle_policy" {
  default = true
}

variable "rollout_job_def" {
  default = ""
}

variable "rollout_job_queue" {
  default = ""
}

variable "create_topic" {
  default = true
}

variable "existing_topic" {
  default     = ""
  description = "SNS Topic ARN to override generated, best to set create_topic to false"
}

variable "scan_on_push" {
  default = true
}